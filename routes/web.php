<?php

Auth::routes();

Route::get('/', 'MenuController@beranda');
Route::get('/2', 'MenuController@menu2');
Route::get('/3', 'MenuController@menu3');
Route::get('/4', 'MenuController@menu4');
Route::get('/5', 'MenuController@menu5');
Route::get('/6', 'MenuController@menu6');

Route::prefix('admin')->group(function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

    Route::resource('content', 'AdminController')->only('edit', 'update');

    Route::resource('menu1', 'Menu1Controller');
    Route::resource('menu1section1', 'Menu1Section1Controller');
    Route::resource('menu1section2', 'Menu1Section2Controller');
    Route::resource('menu1section3', 'Menu1Section3Controller');
});


Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});


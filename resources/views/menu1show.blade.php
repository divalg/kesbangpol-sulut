@extends('layouts.header')

@section('content')
    @include('section.section1')
    @include('section.section2')
    @include('section.section3')
@endsection

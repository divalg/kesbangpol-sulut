<section class="home-slider owl-carousel">

    @foreach ($content->section1 as $k=>$v)
        <div class="slider-item" style="background-image:url({{ $v->image }});">
            <div class="overlay"></div>
            <div class="container">
            <div
                class="row no-gutters slider-text align-items-center justify-content-start"
                data-scrollax-parent="true"
            >
                <div class="col-md-6 ftco-animate">
                <h1 class="mb-4">{{ $v->title }}</h1>
                <p> {{ $v->desc }} </p>
                <p>
                    <a href="#" class="btn btn-primary px-4 py-3 mt-3"
                    >Contact Us</a
                    >
                </p>
                </div>
            </div>
            </div>
        </div>
    @endforeach

</section>

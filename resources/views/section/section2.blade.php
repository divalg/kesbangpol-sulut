<section class="ftco-services ftco-no-pb">
    <div class="container-wrap">
        <div class="row no-gutters">

            @foreach ($content->section2 as $k=>$v)
                <div
                    class="col-md-3 d-flex services align-self-stretch py-5 px-4 ftco-animate {{ $v->id % 2 == 0 ? 'bg-primary' : 'bg-darken' }} "
                >
                    <div class="media block-6 d-block text-center">
                    <div
                        class="icon d-flex justify-content-center align-items-center"
                    >
                        <span class="{{ $v->icon }}"></span>
                    </div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">{{ $v->title }}</h3>
                        <p> {{ $v->desc }} </p>
                    </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section>

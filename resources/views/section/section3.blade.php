<section class="ftco-section ftco-no-pt ftc-no-pb pt-4 pb-4">
    <div class="container">

        @foreach ($content->section3 as $k=>$v)
        <div class="row d-flex">
            <div
            class="col-md-5 order-md-last wrap-about wrap-about d-flex align-items-stretch"
            >
            <div
                class="img"
                style="background-image: url({{ $v->image }}); border"
            ></div>
            </div>
            <div class="col-md-7 wrap-about py-5 pr-md-4 ftco-animate">
            <h2 class="mb-4">{{ $v->parent_title }}</h2>
            <p>{{ $v->parent_desc }}</p>

            <div class="row mt-5">
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-security"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child1_title }}</h3>
                    <p>{{ $v->child1_desc }}</p>
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-reading"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child2_title }}</h3>
                    <p>{{ $v->child2_desc }}</p>
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-diploma"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child3_title }}</h3>
                    <p>{{ $v->child3_desc }}</p>
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-education"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child4_title }}</h3>
                    <p>{{ $v->child4_desc }}</p>
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-jigsaw"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child5_title }}</h3>
                    <p>{{ $v->child5_desc }}</p>
                    </div>
                </div>
                </div>
                <div class="col-lg-6">
                <div class="services-2 d-flex">
                    <div
                    class="icon mt-2 d-flex justify-content-center align-items-center"
                    >
                    <span class="flaticon-kids"></span>
                    </div>
                    <div class="text pl-3">
                    <h3>{{ $v->child6_title }}</h3>
                    <p>{{ $v->child6_desc }}</p>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        @endforeach

    </div>
</section>

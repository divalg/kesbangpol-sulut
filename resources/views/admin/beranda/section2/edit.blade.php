@extends('layouts.app', ['activePage' => 'beranda', 'titlePage' => __('Section 2')])

@section('content')

<style>
    html { overflow-y: scroll; }
</style>

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('menu1section2.update', $data) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            @if (session('error'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('error') }}</span>
                    </div>
                </div>
            </div>
            @endif

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Data') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('menu1.index') }}" class="btn btn-sm btn-primary">{{ __('Kembali') }}</a>
                  </div>
                </div>

                <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                <div class="col-sm-7">
                    <div class="form-group">
                        <input class="form-control" value="{!! $data->title !!}" name="title" type="text" placeholder="{{ __('Judul') }}"/>
                    </div>
                </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <input class="form-control" value="{!! $data->desc !!}" name="desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                        </div>
                    </div>
                </div>

              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>

@endsection

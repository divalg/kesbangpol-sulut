@extends('layouts.app', ['activePage' => 'beranda', 'titlePage' => __('Beranda')])

@section('content')
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('Section 1') }}</h4>
                        </div>
                        <div class="card-body">
                            {{-- @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif --}}
                            <div class="row">
                                <div class="col-12 text-left">
                                    <a href="{{ route('menu1section1.create') }}" class="btn btn-sm btn-primary">{{ __('Tambah Data') }}</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th class="text-center"> {{ __('Gambar') }} </th>
                                        <th class="text-left"> {{ __('Judul') }} </th>
                                        <th class="text-left"> {{ __('Keterangan') }} </th>
                                        <th class="text-center"> {{ __('Act') }} </th>
                                    </thead>
                                    <tbody>
                                    @foreach($section1 as $v)
                                        <tr>
                                            <td class="text-center">
                                                <img src="{{ $v->image }}" height="100">
                                            </td>
                                            <td class="text-left"> {{ $v->title }} </td>
                                            <td class="text-left"> {{ $v->desc }} </td>
                                            <td class="td-actions text-center">
                                                <form action="{{ route('menu1section1.destroy', $v) }}" method="post">
                                                    @csrf
                                                    @method('delete')

                                                    <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('menu1section1.edit', $v) }}" data-original-title="" title="">
                                                        <i class="material-icons">edit</i>
                                                        <div class="ripple-container"></div>
                                                    </a>
                                                    <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Hapus ?") }}') ? this.parentElement.submit() : ''">
                                                        <i class="material-icons">close</i>
                                                        <div class="ripple-container"></div>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('Section 2') }}</h4>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th class="text-left"> {{ __('Judul') }} </th>
                                        <th class="text-left"> {{ __('Keterangan') }} </th>
                                        <th class="text-center"> {{ __('Act') }} </th>
                                    </thead>
                                    <tbody>
                                    @foreach($section2 as $v)
                                        <tr>
                                            <td class="text-left"> {{ $v->title }} </td>
                                            <td class="text-left"> {{ $v->desc }} </td>
                                            <td class="td-actions text-center">
                                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('menu1section2.edit', $v) }}" data-original-title="" title="">
                                                    <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">{{ __('Section 3') }}</h4>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                        <th class="text-center"> {{ __('Image') }} </th>
                                        <th class="text-left"> {{ __('Judul') }} </th>
                                        <th class="text-left"> {{ __('Keterangan') }} </th>
                                        <th class="text-center"> {{ __('Act') }} </th>
                                    </thead>
                                    <tbody>
                                    @foreach($section3 as $v)
                                        <tr>
                                            <td class="text-center">
                                                <img src="/{{ $v->image }}" height="300">
                                            </td>
                                            <td class="text-left"> {{ $v->parent_title }} </td>
                                            <td class="text-left"> {{ $v->parent_desc }} </td>
                                            <td class="td-actions text-center">
                                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('menu1section3.edit', $v) }}" data-original-title="" title="">
                                                    <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection

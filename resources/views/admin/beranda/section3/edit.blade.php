@extends('layouts.app', ['activePage' => 'beranda', 'titlePage' => __('Section 3')])

@section('content')

<style>
    html { overflow-y: scroll; }
</style>

<div class="content">
    <div class="container-fluid row">

        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12 text-left">
                    <a href="{{ route('menu1.index') }}" class="btn btn-sm btn-primary">{{ __('Kembali') }}</a>
                </div>
            </div>

            <form method="post" action="{{ route('menu1section3.update', $data) }}" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
                @csrf
                @method('put')

                <div class="row">
                    <div class="col-md-12">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Parent-Element') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Image') }}</label>
                                    <div class="col-sm-5">
                                        <input type="file" name="image" id="foto" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->parent_title !!}" name="parent_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->parent_desc !!}" name="parent_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 1') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child1_title !!}" name="child1_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child1_desc !!}" name="child1_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 2') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child2_title !!}" name="child2_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child2_desc !!}" name="child2_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 3') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child3_title !!}" name="child3_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child3_desc !!}" name="child3_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 4') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child4_title !!}" name="child4_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child4_desc !!}" name="child4_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 5') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child5_title !!}" name="child5_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child5_desc !!}" name="child5_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Data Child 6') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child6_title !!}" name="child6_title" type="text" placeholder="{{ __('Judul') }}"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <input class="form-control" value="{!! $data->child6_desc !!}" name="child6_desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-block">{{ __('S A V E') }}</button>
                </div>
            </form>

        </div>

        <div class="col-md-4">
            <img id="show" src="{!! $data->image !!}" alt=" No Image" width="100%"/>
        </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            $('#show').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        }
    }

    $("#foto").change(function() {
        readURL(this);
    });

</script>
@endsection

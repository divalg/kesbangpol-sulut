@extends('layouts.app', ['activePage' => 'beranda', 'titlePage' => __('Section 1')])

@section('content')

<style>
    html { overflow-y: scroll; }
</style>

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('menu1section1.update', $data) }}" enctype="multipart/form-data" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            @if (session('error'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('error') }}</span>
                    </div>
                </div>
            </div>
            @endif

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Data') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('menu1.index') }}" class="btn btn-sm btn-primary">{{ __('Kembali') }}</a>
                  </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Image') }}</label>
                    <div class="col-sm-5">
                        <input type="file" name="image" id="foto" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-5">
                        <img id="show" src="{!! $data->image !!}" alt=" No Image" width="100%"/>
                    </div>
                </div>

                <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Judul') }}</label>
                <div class="col-sm-7">
                    <div class="form-group">
                        <input class="form-control" value="{!! $data->title !!}" name="title" type="text" placeholder="{{ __('Judul') }}"/>
                    </div>
                </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Keterangan') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group">
                            <input class="form-control" value="{!! $data->desc !!}" name="desc" type="text" placeholder="{{ __('Keterangan') }}"/>
                        </div>
                    </div>
                </div>

              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            $('#show').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        }
    }

    $("#foto").change(function() {
        readURL(this);
    });

</script>
@endsection

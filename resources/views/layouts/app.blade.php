<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ __('Kesbangpolda Sulut') }}</title>
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material') }}/img/apple-icon.png">
        <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="{{ asset('css') }}/font-material-icons.css" rel="stylesheet" />
        <link href="{{ asset('css') }}/font-awesome.min.css" rel="stylesheet" />
        <!-- CSS Files -->
        <link href="{{ asset('material') }}/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="{{ asset('datepicker') }}/bootstrap-material-datetimepicker.css" rel="stylesheet" />


        <!--   Core JS Files   -->
        <script src="{{ asset('material') }}/js/core/jquery.min.js"></script>
        <script src="{{ asset('material') }}/js/core/popper.min.js"></script>
        <script src="{{ asset('material') }}/js/core/bootstrap-material-design.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/moment.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/sweetalert2.js"></script>
        <script src="{{ asset('material') }}/js/plugins/jquery.validate.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/jquery.bootstrap-wizard.js"></script>
        <script src="{{ asset('material') }}/js/plugins/bootstrap-selectpicker.js"></script>
        {{-- <script src="{{ asset('material') }}/js/plugins/bootstrap-datetimepicker.min.js"></script> --}}
        <script src="{{ asset('datepicker') }}/bootstrap-material-datetimepicker.js"></script>
        <script src="{{ asset('datepicker') }}/moment.js"></script>
        <script src="{{ asset('material') }}/js/plugins/jquery.dataTables.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/bootstrap-tagsinput.js"></script>
        <script src="{{ asset('material') }}/js/plugins/jasny-bootstrap.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/fullcalendar.min.js"></script>
        <script src="{{ asset('material') }}/js/plugins/jquery-jvectormap.js"></script>
        <script src="{{ asset('material') }}/js/plugins/nouislider.min.js"></script>
        <script src="{{ asset('js') }}/core-2.4.1.js"></script>
        <script src="{{ asset('material') }}/js/plugins/arrive.min.js"></script>

        <!-- Chartist JS -->
        <script src="{{ asset('material') }}/js/plugins/chartist.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="{{ asset('material') }}/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="{{ asset('material') }}/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
        <!-- Material Dashboard DEMO methods, don't include it in your project! -->
        <script src="{{ asset('material') }}/demo/demo.js"></script>
        <script src="{{ asset('material') }}/js/settings.js"></script>
        @stack('js')
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.page_templates.auth')
        @endauth
        @guest()
            @include('layouts.page_templates.guest')
        @endguest

    </body>
</html>

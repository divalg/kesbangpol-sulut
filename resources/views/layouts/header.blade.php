<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kesbangpol</title>
        <meta charset="utf-8" />
        <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />

        <link
        href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap"
        rel="stylesheet"
        />

        <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}" />

        <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" />

        <link rel="stylesheet" href="{{ asset('css/aos.css') }}" />

        <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}" />

        <link rel="stylesheet" href="{{ asset('css/flaticon.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}" />

        <script src="{{ asset('js/jquery.min.js') }}" defer></script>
        <script src="{{ asset('js/jquery-migrate-3.0.1.min.js') }}" defer></script>
        <script src="{{ asset('js/popper.min.js') }}" defer></script>
        <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
        <script src="{{ asset('js/jquery.easing.1.3.js') }}" defer></script>
        <script src="{{ asset('js/jquery.waypoints.min.js') }}" defer></script>
        <script src="{{ asset('js/jquery.stellar.min.js') }}" defer></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}" defer></script>
        <script src="{{ asset('js/jquery.magnific-popup.min.js') }}" defer></script>
        <script src="{{ asset('js/aos.js') }}" defer></script>
        <script src="{{ asset('js/jquery.animateNumber.min.js') }}" defer></script>
        <script src="{{ asset('js/scrollax.min.js') }}" defer></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
        <script src="{{ asset('js/google-map.js') }}" defer></script>
        <script src="{{ asset('js/main.js') }}" defer></script>
    </head>

    <body>
        <nav
            class="navbar navbar-expand-lg navbar-dark bg-danger"
            id="ftco-navbar"
            >
            <div class="container d-flex align-items-center px-4">
                <div class="shadow-lg p-0 bg-transparent rounded">
                    <img src="/images/logo2.png" alt="" height="120">
                </div>
                <div class="shadow-lg p-0 bg-transparent rounded">
                    <h1 class="text-white">KESBANGPOLDA SULUT</h1>
                    <p>Lorem Ipsum</p>
                </div>
                <div class="shadow-lg p-0 bg-transparent rounded">
                    <img src="/images/logo1.png" alt="" height="120">
                </div>
            </div>
        </nav>
        <nav
        class="navbar navbar-expand-lg navbar-dark bg-dark"
        id="ftco-navbar"
        >
            <div class="container d-flex align-items-center px-4">
                <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#ftco-nav"
                    aria-controls="ftco-nav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                <span class="oi oi-menu"></span> Menu
                </button>
                {{-- <form action="#" class="searchform order-lg-last">
                <div class="form-group d-flex">
                    <input type="text" class="form-control pl-3" placeholder="Search" />
                    <button type="submit" placeholder="" class="form-control search">
                    <span class="ion-ios-search"></span>
                    </button>
                </div>
                </form> --}}
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav mr-auto" style="font-size: 24px;">
                        <li class="nav-item pr-2 {{ $x == 1 ? 'active' : '' }}">
                            <a href="/" class="nav-link pl-0">Beranda</a>
                        </li>
                        <li class="nav-item pr-2 {{ $x == 2 ? 'active' : '' }}">
                            <a href="/2" class="nav-link">Berita</a>
                        </li>
                        <li class="nav-item dropdown {{ $x == 3 ? 'active' : '' }}">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                                Profil
                            </a>
                            <ul class="dropdown-menu mt-0" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Visi & Misi</a></li>
                                <li><a class="dropdown-item" href="#">Tugas Pokok & Fungsi</a></li>
                                <li><a class="dropdown-item" href="#">Struktur Organisasi</a></li>
                            </ul>
                        </li>
                        <li class="nav-item pr-2 {{ $x == 4 ? 'active' : '' }}">
                            <a href="/4" class="nav-link">Regulasi</a>
                        </li>
                        <li class="nav-item pr-2 {{ $x == 5 ? 'active' : '' }}">
                            <a href="/5" class="nav-link">Pertanyaan</a>
                        </li>
                        <li class="nav-item {{ $x == 6 ? 'active' : '' }}">
                            <a href="/6" class="nav-link">Download</a>
                        </li>
                        <li class="nav-item {{ $x == 7 ? 'active' : '' }}">
                            <a href="/6" class="nav-link">Galeri</a>
                        </li>
                        <li class="nav-item {{ $x == 8 ? 'active' : '' }}">
                            <a href="/6" class="nav-link">Kontak</a>
                        </li>
                    </ul>
                </div>

                <div class="collapse navbar-collapse order-lg-last" id="ftco-nav">
                    <ul class="navbar-nav ml-auto" style="font-size: 24px;">
                        <li class="nav-item pr-2 active">
                            <a href="/admin" class="nav-link pl-0">Login</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END nav -->

        @yield('content')

        @include('layouts.footer')

        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen">
        <svg class="circular" width="48px" height="48px">
            <circle
            class="path-bg"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            stroke-width="4"
            stroke="#eeeeee"
            />
            <circle
            class="path"
            cx="24"
            cy="24"
            r="22"
            fill="none"
            stroke-width="4"
            stroke-miterlimit="10"
            stroke="#F96D00"
            />
        </svg>
        </div>
    </body>
</html>

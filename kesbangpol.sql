-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for web_kesbangpol
CREATE DATABASE IF NOT EXISTS `web_kesbangpol` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `web_kesbangpol`;

-- Dumping structure for table web_kesbangpol.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.menu1_section1s
CREATE TABLE IF NOT EXISTS `menu1_section1s` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.menu1_section1s: ~2 rows (approximately)
/*!40000 ALTER TABLE `menu1_section1s` DISABLE KEYS */;
INSERT INTO `menu1_section1s` (`id`, `image`, `title`, `desc`, `created_at`, `updated_at`) VALUES
	(6, '/images/menu1section1/2020-02-29_012900.jpg', 'Education Needs Complete Solution', 'A small river named Duden flows by their place and supplies it with the necessary regelialia.', '2020-02-29 13:29:00', '2020-02-29 13:29:00'),
	(7, '/images/menu1section1/2020-02-29_013008.jpg', 'University, College School Education', 'A small river named Duden flows by their place and supplies it with the necessary regelialia.', '2020-02-29 13:30:08', '2020-02-29 13:30:08');
/*!40000 ALTER TABLE `menu1_section1s` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.menu1_section2s
CREATE TABLE IF NOT EXISTS `menu1_section2s` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.menu1_section2s: ~4 rows (approximately)
/*!40000 ALTER TABLE `menu1_section2s` DISABLE KEYS */;
INSERT INTO `menu1_section2s` (`id`, `icon`, `title`, `desc`, `created_at`, `updated_at`) VALUES
	(1, 'flaticon-teacher', 'Certified Teachers', 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.', NULL, NULL),
	(2, 'flaticon-reading', 'Special Education', 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.', NULL, NULL),
	(3, 'flaticon-books', 'Book and Library x', 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic', NULL, '2020-02-29 13:33:30'),
	(4, 'flaticon-diploma', 'Sport Clubs', 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic', NULL, NULL);
/*!40000 ALTER TABLE `menu1_section2s` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.menu1_section3s
CREATE TABLE IF NOT EXISTS `menu1_section3s` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_desc` text COLLATE utf8mb4_unicode_ci,
  `child1_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child1_desc` text COLLATE utf8mb4_unicode_ci,
  `child2_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child2_desc` text COLLATE utf8mb4_unicode_ci,
  `child3_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child3_desc` text COLLATE utf8mb4_unicode_ci,
  `child4_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child4_desc` text COLLATE utf8mb4_unicode_ci,
  `child5_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child5_desc` text COLLATE utf8mb4_unicode_ci,
  `child6_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child6_desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.menu1_section3s: ~0 rows (approximately)
/*!40000 ALTER TABLE `menu1_section3s` DISABLE KEYS */;
INSERT INTO `menu1_section3s` (`id`, `image`, `parent_title`, `parent_desc`, `child1_title`, `child1_desc`, `child2_title`, `child2_desc`, `child3_title`, `child3_desc`, `child4_title`, `child4_desc`, `child5_title`, `child5_desc`, `child6_title`, `child6_desc`, `created_at`, `updated_at`) VALUES
	(1, '/images/menu1section3/about.jpg', 'What We Offer', 'On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word.', 'Safety Firstx', 'Far far away, behind the word mountains, far from the countries Vokalia.', 'Regular Classesx', 'Far far away, behind the word mountains, far from the countries Vokalia.', 'Certified Teachersx', 'Far far away, behind the word mountains, far from the countries Vokalia.', 'Sufficient Classroomsx', 'Far far away, behind the word mountains, far from the countries Vokalia.', 'Creative Lessons', 'Far far away, behind the word mountains, far from the countries Vokalia.', 'Sports Facilities', 'Far far away, behind the word mountains, far from the countries Vokalia.', NULL, '2020-02-29 14:05:40');
/*!40000 ALTER TABLE `menu1_section3s` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.migrations: ~13 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(14, '2014_10_12_000000_create_users_table', 1),
	(15, '2014_10_12_100000_create_password_resets_table', 1),
	(16, '2019_08_19_000000_create_failed_jobs_table', 1),
	(17, '2020_01_12_175857_create_menu1_section1s_table', 1),
	(18, '2020_01_12_181620_create_menu1_section2s_table', 1),
	(19, '2020_01_12_183307_create_menu1_section3s_table', 1),
	(20, '2020_01_12_191734_create_menu1_section4s_table', 1),
	(21, '2020_01_12_191741_create_menu1_section5s_table', 1),
	(22, '2020_01_12_191747_create_menu1_section6s_table', 1),
	(23, '2020_01_12_191753_create_menu1_section7s_table', 1),
	(24, '2020_01_12_191800_create_menu1_section8s_table', 1),
	(25, '2020_01_12_191807_create_menu1_section9s_table', 1),
	(26, '2020_01_12_191814_create_menu1_section10s_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table web_kesbangpol.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table web_kesbangpol.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'admin@mail.com', '2020-02-18 05:03:05', '$2y$10$KWbUBzYYWgkdYJYkk910KujL1.9JmI/CZv22o7BwCI9x/.n.8cyKO', NULL, '2020-02-18 05:03:06', '2020-02-18 05:03:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

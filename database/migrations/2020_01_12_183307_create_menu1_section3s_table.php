<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenu1Section3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu1_section3s', function (Blueprint $table) {
            $table->bigIncrements('id');

                $table->string('image')->nullable();
                $table->string('parent_title')->nullable();
                $table->text('parent_desc')->nullable();

                $table->string('child1_title')->nullable();
                $table->text('child1_desc')->nullable();
                $table->string('child2_title')->nullable();
                $table->text('child2_desc')->nullable();
                $table->string('child3_title')->nullable();
                $table->text('child3_desc')->nullable();
                $table->string('child4_title')->nullable();
                $table->text('child4_desc')->nullable();
                $table->string('child5_title')->nullable();
                $table->text('child5_desc')->nullable();
                $table->string('child6_title')->nullable();
                $table->text('child6_desc')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu1_section3s');
    }
}

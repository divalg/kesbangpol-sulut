<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenu1Section2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu1_section2s', function (Blueprint $table) {
            $table->bigIncrements('id');

                $table->string('icon', 100)->nullable();
                $table->string('title', 100)->nullable();
                $table->string('desc', 100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu1_section2s');
    }
}

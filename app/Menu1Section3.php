<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu1Section3 extends Model
{
    protected $fillable = [
        'image',
        'parent_title',
        'parent_desc',
        'child1_title',
        'child1_desc',
        'child2_title',
        'child2_desc',
        'child3_title',
        'child3_desc',
        'child4_title',
        'child4_desc',
        'child5_title',
        'child5_desc',
        'child6_title',
        'child6_desc',
    ];
}

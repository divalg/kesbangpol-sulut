<?php

namespace App\Http\Controllers;

use App\Menu1Section1;
use Illuminate\Http\Request;

class Menu1Section1Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        return view('admin.beranda.section1.create');
    }

    public function store(Request $r)
    {
        $data = $r->validate([
            'title' => 'required',
            'desc' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $a = Menu1Section1::make($data);
        if ($r->hasFile('image')) {
            $imageName = date('Y-m-d_his').'.'.$r->image->getClientOriginalExtension();
            $folder = '/images/menu1section1/';
            $r->image->move(public_path($folder), $imageName);
            $a->image = $folder.$imageName;
        }
        $a->save();

        return redirect('admin/menu1');
    }

    public function edit($id)
    {
        $data = Menu1Section1::find($id);
        return View('admin.beranda.section1.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'title' => 'required',
            'desc' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $a = Menu1Section1::find($id);

        if ($r->hasFile('image')) {
            $img = explode("/", $a->image);
            $imageName = $img[3];
            $folder = "/" .$img[1]. "/" .$img[2]. "/";
            $r->image->move(public_path($folder), $imageName);
        }

        $a->update($data);
        return redirect('admin/menu1');
    }

    public function destroy($id)
    {
        Menu1Section1::destroy($id);
        return redirect('admin/menu1');
    }
}

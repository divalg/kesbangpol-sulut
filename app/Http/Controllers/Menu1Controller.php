<?php

namespace App\Http\Controllers;

use App\Menu1Section1;
use App\Menu1Section2;
use App\Menu1Section3;
use Illuminate\Http\Request;

class Menu1Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $section1 = Menu1Section1::all();
        $section2 = Menu1Section2::all();
        $section3 = Menu1Section3::all();

        return view('admin.beranda.index', compact('section1', 'section2', 'section3'));
    }

    public function berandaUpdate(Request $request)
    {
        //
    }

    private function berandaInsert($id)
    {

    }
}

<?php

namespace App\Http\Controllers;

use App\Menu1Section3;
use Illuminate\Http\Request;

class Menu1Section3Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $data = Menu1Section3::find($id);
        return View('admin.beranda.section3.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'parent_title' => 'required',
            'parent_desc' => 'required',
            'child1_title' => 'required',
            'child1_desc' => 'required',
            'child2_title' => 'required',
            'child2_desc' => 'required',
            'child3_title' => 'required',
            'child3_desc' => 'required',
            'child4_title' => 'required',
            'child4_desc' => 'required',
            'child5_title' => 'required',
            'child5_desc' => 'required',
            'child6_title' => 'required',
            'child6_desc' => 'required',
        ]);

        $a = Menu1Section3::find($id);

        if ($r->hasFile('image')) {
            $img = explode("/", $a->image);
            $imageName = $img[3];
            $folder = "/" .$img[1]. "/" .$img[2]. "/";
            $r->image->move(public_path($folder), $imageName);
        }

        $a->update($data);
        return redirect('admin/menu1');
    }

}

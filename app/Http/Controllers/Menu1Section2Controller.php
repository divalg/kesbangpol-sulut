<?php

namespace App\Http\Controllers;

use App\Menu1Section2;
use Illuminate\Http\Request;

class Menu1Section2Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        $data = Menu1Section2::find($id);
        return View('admin.beranda.section2.edit', compact('data'));
    }

    public function update(Request $r, $id)
    {
        $data = $r->validate([
            'title' => 'required',
            'desc' => 'required',
        ]);

        $a = Menu1Section2::find($id);
        $a->update($data);

        return redirect('admin/menu1');
    }

}

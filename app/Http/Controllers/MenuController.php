<?php

namespace App\Http\Controllers;

use App\Menu1Section1;
use App\Menu1Section2;
use App\Menu1Section3;

class MenuController extends Controller
{
    public function index($x, $content)
    {
        return View('menu'.$x.'show', compact('x', 'content'));
    }

    public function beranda()
    {
        $section1 = Menu1Section1::all();
        $section2 = Menu1Section2::all();
        $section3 = Menu1Section3::all();

        $content = (Object)[
            'section1' => $section1,
            'section2' => $section2,
            'section3' => $section3,
        ];

        return $this->index(1, $content);
    }

    public function menu2()
    {
        $content = (Object)[
        ];

        return $this->index(2, $content);
    }

    public function menu3()
    {
        $content = (Object)[
        ];

        return $this->index(3, $content);
    }

    public function menu4()
    {
        $content = (Object)[
        ];

        return $this->index(4, $content);
    }

    public function menu5()
    {
        $content = (Object)[
        ];

        return $this->index(5, $content);
    }

    public function menu6()
    {
        $content = (Object)[
        ];

        return $this->index(6, $content);
    }
}

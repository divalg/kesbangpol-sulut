<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu1Section2 extends Model
{
    protected $fillable = ['icon', 'title', 'desc'];
}

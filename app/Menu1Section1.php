<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu1Section1 extends Model
{
    protected $fillable = ['image', 'title', 'desc'];
}
